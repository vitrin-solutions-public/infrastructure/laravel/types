<?php

namespace Vitrin\Infrastructure\Types;

class Numberable
{
    public function __construct(
        public float $value,
    ) {
    }

    public function __toString()
    {
        return (string) $this->value;
    }
}
