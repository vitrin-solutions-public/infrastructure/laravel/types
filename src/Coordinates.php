<?php

namespace Vitrin\Infrastructure\Types;

class Coordinates
{
    public function __construct(public float $lat, public float $lng)
    {

    }

    public function toArray()
    {
        return [
            'lat'   => $this->lat,
            'lng'   => $this->lng,
        ];
    }
}
