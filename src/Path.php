<?php

namespace Vitrin\Infrastructure\Types;

class Path
{
    public function __construct(public string $path)
    {

    }

    public function value()
    {
        return $this->path;
    }

    public function __toString()
    {
        return (string) $this->path;
    }
}
