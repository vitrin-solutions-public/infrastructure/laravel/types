<?php

namespace Vitrin\Infrastructure\Types;

use Spatie\LaravelData\Casts\Cast;
use Spatie\LaravelData\Support\Creation\CreationContext;
use Spatie\LaravelData\Support\DataProperty;
use Spatie\LaravelData\Support\Transformation\TransformationContext;
use Spatie\LaravelData\Transformers\Transformer;

class LocalKeyMapper implements Cast, Transformer
{
    public function cast(DataProperty $property, mixed $value, array $properties, CreationContext $context): LocalKey
    {
        try {
            return new LocalKey($value, $property->attributes->first()->arguments[0]);
        } catch (\Throwable $th) {
            return new LocalKey($value, null);
        }
    }

    public function transform(DataProperty $property, mixed $value, TransformationContext $context): int
    {
        return $value->__toString();
    }
}
