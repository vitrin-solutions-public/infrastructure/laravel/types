<?php

namespace Vitrin\Infrastructure\Types;

use Carbon\Carbon;

class Date
{
    public Carbon $date;

    public function __construct(string $date ) {
        $this->date = new Carbon($date);
    }

    public function isPast(): bool
    {
        return $this->date->isPast();
    }

    public function isExpired(): bool
    {
        return $this->isPast();
    }

    public function isFuture(): bool
    {
        return $this->date->isFuture();
    }
    
    public function ago(): string
    {
        return $this->date->diffForHumans();
    }
}