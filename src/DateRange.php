<?php

namespace Vitrin\Infrastructure\Types;

use Carbon\Carbon;

class DateRange
{
    public function __construct(
        public ?string $from = null,
        public ?string $to = null,
    ) {

    }

    public function hasStart()
    {
        return !!$this->from;
    }

    public function hasEnd()
    {
        return !!$this->to;
    }

    public function from(): Carbon
    {
        return new Carbon($this->from);
    }

    public function to(): Carbon
    {
        return new Carbon($this->to);
    }

    public function toArray()
    {
        return [
            'from'  => $this->from,
            'to'    => $this->to
        ];
    }
}
