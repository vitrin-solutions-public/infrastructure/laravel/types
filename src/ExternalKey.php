<?php

namespace Vitrin\Infrastructure\Types;

class ExternalKey
{
    public function __construct(public int $key)
    {

    }

    public function __toString()
    {
        return (string) $this->key;
    }
}
