<?php

namespace Vitrin\Infrastructure\Types;

use Illuminate\Support\Str;

class PhoneNumber
{
    public function __construct(public string $phone_number)
    {

    }

    public function countryCode()
    {
        return '+98';
    }

    public function masked()
    {
        return Str::mask($this->phone_number, '****', 4);
    }
}
