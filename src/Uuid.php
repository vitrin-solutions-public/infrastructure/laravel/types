<?php

namespace Vitrin\Infrastructure\Types;

class Uuid
{
    public function __construct(public string $uuid)
    {

    }

    public function value()
    {
        return $this->uuid;
    }

    public function __toString()
    {
        return $this->uuid;
    }
}
