<?php

namespace Vitrin\Infrastructure\Types;

class LocalKey
{
    public function __construct(public int $key, public ?string $table = null)
    {

    }

    public function getIdentifier(): int
    {
        return $this->key;
    }

    public function __toString()
    {
        return (string) $this->key;
    }
}
