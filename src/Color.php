<?php

namespace Vitrin\Infrastructure\Types;

class Color
{
    public function __construct(
        public string $value,
    ) {
    }

    public function toHex()
    {
        return $this->value;
    }

    public function __toString()
    {
        return $this->value;
    }
}