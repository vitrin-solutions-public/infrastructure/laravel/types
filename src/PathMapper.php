<?php

namespace Vitrin\Infrastructure\Types;

use Spatie\LaravelData\Casts\Cast;
use Spatie\LaravelData\Support\Creation\CreationContext;
use Spatie\LaravelData\Support\DataProperty;
use Spatie\LaravelData\Support\Transformation\TransformationContext;
use Spatie\LaravelData\Transformers\Transformer;

class PathMapper implements Cast, Transformer
{
    public function cast(DataProperty $property, mixed $value, array $properties, CreationContext $context): Path
    {
        return new Path($value);
    }

    public function transform(DataProperty $property, mixed $value, TransformationContext $context): string
    {
        return $value->__toString();
    }
}
