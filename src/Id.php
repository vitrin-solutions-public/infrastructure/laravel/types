<?php

namespace Vitrin\Infrastructure\Types;

class Id
{
    public function __construct(public int $id, public ?string $prefix = null)
    {

    }

    public function value()
    {
        return $this->id;
    }

    public function label()
    {
        return $this->prefix . '-' . $this->id;
    }

    public function __toString()
    {
        return (string) $this->id;
    }
}
